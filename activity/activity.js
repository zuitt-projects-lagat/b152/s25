//Total number of items supplied by Red Farms:
 db.fruits.aggregate([

    {$match: {supplier: "Red Farms Inc."}},
    {$count: "name"},
    {$out: "redFarmItems"}

])



 //Number of items with price above 50:
 db.fruits.aggregate([

    {$match: {price: {$gt: 50}}},
    {$count: "name"},
    {$out: "greaterThan50Items"}

])



 //Average price on sale:
 db.fruits.aggregate([

    {$match: {onSale: true}},
    {$group: {_id:"avgPriceOnSale", avgPrice: {$avg: "$price"}}},

])



 //Highest price of item per supplier:
 db.fruits.aggregate([

    {$match: {onSale: true}},
    {$group: {_id:"$supplier", highestPrice: {$max: "$price"}}},

])



 //Lowest price of item per supplier:
 db.fruits.aggregate([

    {$match: {onSale: true}},
    {$group: {_id:"$supplier", lowestPrice: {$min: "$price"}}},

])